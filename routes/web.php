<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('/');

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');    
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::middleware('auth')->group(function () {
    Route::get('/home', function () {
        return Inertia::render('HomeComponent');
    })->name('home');

    Route::get('/about', function () {
        return Inertia::render('AboutComponent');
    })->name('about');

    Route::get('/activity-log', function () {
        return Inertia::render('ActivityComponent');
    })->name('activity-log');

    Route::get('/maps', function () {
        return Inertia::render('GoogleMapsComponent');
    })->name('maps');

    Route::get('/time', function () {
        return Inertia::render('TimeComponent');
    })->name('time');

    Route::get('/show', [HomeController::class, 'getDetails'])->name('show');

    Route::get('/users', [HomeController::class, 'getUsers'])->name('users');

    Route::get('/export', [HomeController::class, 'exportUsers'])->name('export');

    Route::get('/activity_log', [HomeController::class, 'userActivityLog'])->name('activity_log');

    Route::get('/last_activity', [HomeController::class, 'lastActivity'])->name('last_activity');

    Route::post('/view', [HomeController::class, 'view'])->name('view');
});

Route::middleware('auth', 'admin')->group(function () {
    Route::get('/admin', function () {
        return Inertia::render('Admin/AdminComponent');
    })->name('admin');
});

require __DIR__ . '/auth.php';
