import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout.vue';
import { Head } from '@inertiajs/inertia-vue3';

export default {
    data() {
        return {
            details: [],
            users: [],
            show: false,
        }
    },
    components: {
        AuthenticatedLayout,
        Head
    },
    created() {
        this.fetchDetails()
    },
    methods: {
        async fetchDetails() {
            try {
                let response = await axios.get(route('show'));
                this.details = response.data;
            }
            catch (error) {
                alert(error);
            }
        },
        async getUsers() {
            try {
                let response = await axios.get(route('users'));
                this.show = true;
                this.users = response.data;
            }
            catch (error) {
                alert(error);
            }
        },
        async exportData() {
            try {
                let response = await axios.get(route('export'), { responseType: 'blob' });
                const url = URL.createObjectURL(new Blob([response.data], {
                    type: 'application/vnd.ms-excel'
                }))
                console.log(url);
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', 'users.xlsx')
                document.body.appendChild(link)
                link.click()
                alert("Data exported successfully");
            }
            catch (error) {
                alert(error);
            }
        },
    }
}
