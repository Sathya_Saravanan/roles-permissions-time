<?php

namespace App\Http\Controllers;

use App\Exports\ExportUser;
use App\Models\User;
use App\Repositories\Common\CommonRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Activitylog\Models\Activity as ModelsActivity;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        // $this->userRepo=new CommonRepository($user);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->user_type === 1) {
            activity('admin')
                ->causedBy($user)
                ->event('verified')
                ->log($user->name . ' logged in');
        } else {
            activity('user')
                ->causedBy($user)
                ->event('verified')
                ->log($user->name . ' logged in');
        }

        return Inertia::render('Dashboard');
    }

    public function getDetails()
    {
        $user = User::where('name', Auth::user()->name)->get();
        return $user;
    }

    public function getUsers()
    {
        $users = User::all();
        return $users;
    }

    public function exportUsers()
    {
        return Excel::download(new ExportUser, 'users.xlsx');
    }

    public function authentication(Request $request)
    {
        if (Auth::user()->id == $request->a && Auth::user()->name == $request->b) {
            return redirect('content');
        } else {
            return redirect('time');
        }
    }
    public function collect()
    {
        $details = Auth::user();
        return view('iframe', compact('details'));
    }

    public function userActivityLog()
    {
        $data = ModelsActivity::all();
        return $data;
    }

    public function lastActivity()
    {
        $activity = ModelsActivity::all()->last();
        $activity = Crypt::encryptString($activity);
        return $activity;
    }

    public function view(Request $request)
    {
        try {
            $decrypt = Crypt::decryptString($request->id);
            return $decrypt;
        } catch (DecryptException $th) {
            throw $th;
        }
    }
}
