<?php

namespace App\Repositories\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

// Repo

class CommonRepository implements CommonInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function GetDataById($id)
    {
        return $this->model->find($id);
    }

    public function get($where = null, $page = 1, $limit = 30, $orderKey = 'id', $order = 'DESC')
    {
        $data = $this->model;
        if (! is_null($where)) {
            $data = $data->where($where);
        }

        return $data = $data->limit($limit)->offset(($page - 1) * $limit)->orderBy($orderKey, $order)->get();
    }

    public function findOneBy(array $criteria)
    {
        return $this->model->where($criteria)->first();
    }

    public function show($id)
    {
        try {
            return $this->model->find($id);
        } catch (ModelNotFoundException $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function store($data)
    {
        try {
            return $this->model->create($data);
        } catch (ModelNotFoundException $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function update($data, $id)
    {
        try {
            $record = $this->model->find($id);

            return $record->update($data);
        } catch (ModelNotFoundException $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $record = $this->model->find($id);

            return $record->update(['delete_status' => 1]);
        } catch (ModelNotFoundException $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function create(array $insertData)
    {
        try {
            return $this->model->create($insertData);
        } catch (ModelNotFoundException $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function getAll($select = '*')
    {
        return $this->model->select($select)->get();
    }

    public function where(array $criteria, $select = '*')
    {
        return $this->model->where($criteria)->select($select)->get();
    }

    public function wherePluck(array $criteria, string $pluckVariable)
    {
        return $this->model->where($criteria)->pluck($pluckVariable)->first();
    }

    public function whereInorderBy(array $criteria, $type = 'ASC', $column = 'id')
    {
        return $this->model->where($criteria)->orderBy($column, $type)->get();
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    public function findOneByActive(array $criteria)
    {
        return $this->model->where($criteria)->where('active_status', 1)->where('delete_status', 0)->first();
    }

    public function findAllByActive(array $criteria)
    {
        return $this->model->where($criteria)->where('active_status', 1)->where('delete_status', 0)->get();
    }

    public function hardDelete($id)
    {
        try {
            return $this->model->find($id)->delete();
        } catch (ModelNotFoundException $exception) {
            abort(500, $exception->getMessage());
        }
    }

    public function checkTopicNameExists(string $name, int $id)
    {
        return $this->model->where('name', 'LIKE', $name)->whereNotIn('id', [$id])->where('active_status', 1)->where('delete_status', 0)->get();
    }

    public function count()
    {
        return $this->model->count();
    }
}
