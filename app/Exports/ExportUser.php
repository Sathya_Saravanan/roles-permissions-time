<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\Auth;

class ExportUser implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'E-mail ID',
            'Created_At',
            'Updated_At'
        ];
    }

    public function collection()
    {
        $user = User::where('user_type', 0)->get();
        return $user;
    }

    public function map($row): array
    {
        $fields = [
            $row->id,
            $row->name,
            $row->email,
            $row->created_at,
            $row->updated_at            
        ];

        return $fields;
    }
}
